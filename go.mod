module modernc.org/ssa

go 1.19

require (
	github.com/dustin/go-humanize v1.0.1
	modernc.org/mathutil v1.5.0
)

require github.com/remyoudompheng/bigfft v0.0.0-20200410134404-eec4a21b6bb0 // indirect
