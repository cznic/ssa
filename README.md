# ssa

Package SSA handles transformations of the single static assignment form.

See: https://en.wikipedia.org/wiki/Static_single_assignment_form